" Plug section
call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/syntastic'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'bling/vim-airline'
Plug 'airblade/vim-gitgutter'
Plug 'rust-lang/rust.vim'
Plug 'scrooloose/nerdcommenter'
call plug#end()

set nocompatible

" Some dotfile inspiration was pulled from https://github.com/skwp/dotfiles/blob/master/vimrc

" ==== General config ====

set number " show line numbers
set backspace=indent,eol,start
set history=1000
set showcmd
set showmode
set visualbell " no annoying audio bell

set hidden " Buffers can exist in the background without being in a window
syntax on
colorscheme elflord

" ==== Indentation ====

set autoindent
set smartindent
set smarttab
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab
set nowrap " Don't wrap lines
set linebreak " wrap lines at convenient points

" show whitespace

set list
set listchars=tab:>·,trail:·

" ==== line breaks ===

set wrap " word wrap visually
set linebreak " don't wrap inside words
set nolist " turns off physical line wrapping
set textwidth=0 
set wrapmargin=0

" === fix j and k going through line breaks ===

noremap <buffer> <silent> k gk
noremap <buffer> <silent> j gj
noremap <buffer> <silent> 0 g0
noremap <buffer> <silent> $ g$

" ==== Completion ====

set wildmode=list:longest
set wildmenu

" ==== Scrolling ====

set scrolloff=8 " start scrolling 8 lines from end of file
set sidescrolloff=15
set sidescroll=1

" ==== Search ====

set incsearch
set hlsearch
set ignorecase
set smartcase

" ==== Other stuff ====

:map <F7> :tabp<Enter>
:map <F8> :tabn<Enter>

" ==== whitespace ====

set list
set listchars=tab:>·,trail:·

" ==== nicer colors? ====

set termguicolors

" ==== rust ====

let g:rustfmt_autosave = 1

" ==== shortcuts, other config ====

map <C-n> :NERDTreeToggle<CR> " nerd tree with ctrl+n
let g:NERDSpaceDelims = 1 " add space after comment character
let g:NERDCompactSecyComs = 1 " better formatting
let g:NERDDefaultAlign = 'left' " left align all comment characters
