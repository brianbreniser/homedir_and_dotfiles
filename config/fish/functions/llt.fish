function llt --description 'List directory content tree, but use lsd instead'
    lsd --tree $argv
end
