function vimf --description 'vim with fzf'
    vim (fzf) $argv
end
