function ll --description 'List contents but use lsd instead'
    lsd -lF $argv
end
